package si.uni_lj.fri.pbd.miniapp3.ui.favourites;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import java.util.ArrayList;
import java.util.List;

import si.uni_lj.fri.pbd.miniapp3.R;
import si.uni_lj.fri.pbd.miniapp3.adapter.RecyclerViewAdapter;
import si.uni_lj.fri.pbd.miniapp3.adapter.SpinnerAdapter;
import si.uni_lj.fri.pbd.miniapp3.database.entity.RecipeDetails;
import si.uni_lj.fri.pbd.miniapp3.models.Mapper;
import si.uni_lj.fri.pbd.miniapp3.models.RecipeSummaryIM;
import si.uni_lj.fri.pbd.miniapp3.ui.MainViewModel;

public class FavoritesFragment extends Fragment {

    private static final String TAG = "Favorites Fragment";

    //Initialize variables

    private MainViewModel mViewModel;

    private RecyclerView recyclerView;
    private RecyclerView.LayoutManager layoutManager;
    private RecyclerViewAdapter recAdapter;
    View view;
    public List<RecipeSummaryIM> recipeSummaryList;
    public ProgressBar bar;


    //empty constructor
    public FavoritesFragment(){

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        view = inflater.inflate(R.layout.fragment_favorites,container,false);
        mViewModel = new ViewModelProvider(this).get(MainViewModel.class);
        recyclerView = view.findViewById(R.id.favorite);
        layoutManager = new LinearLayoutManager(view.getContext());
        recyclerView.setLayoutManager(layoutManager);

        //map RecipeDetails in RecipeSummaryIM
        List<RecipeDetails> recipeDetails=mViewModel.getFaveFragment();

        recipeSummaryList=new ArrayList<RecipeSummaryIM>();
        for(int i=0; i<recipeDetails.size();i++){
            Log.i(TAG,recipeDetails.toString());
            recipeSummaryList.add(Mapper.mapRecipeDetailsToRecipeSummaryIm(recipeDetails.get(i)));
        }

        //notify the adapter about the surce
        recAdapter.setSource("favorites");
        recAdapter = new RecyclerViewAdapter(getContext(), recipeSummaryList, bar);
        //new RecyclerViewAdapter().setRecipeList(recipeSummaryList);
        recyclerView.setAdapter(recAdapter);

        //observer for adding and deleting items in favorites
        mViewModel.getFavorites().observe(getViewLifecycleOwner(),
        new Observer<List<RecipeDetails>>() {
            @Override
            public void onChanged(@Nullable final List<RecipeDetails> recipes) {
                recipeSummaryList=new ArrayList<RecipeSummaryIM>();
                for(int i=0; i<recipes.size();i++){
                    recipeSummaryList.add(Mapper.mapRecipeDetailsToRecipeSummaryIm(recipes.get(i)));
                }
                recAdapter.setRecipeList(recipeSummaryList);
                recAdapter.setSource("favorites");

            };
        });
        return view;

    }
//set source for adapter
    @Override
    public void onResume() {
        super.onResume();
        if(recipeSummaryList.size()==0){
            Toast.makeText(getContext(), "There are no recipes marked as Favorites", Toast.LENGTH_LONG).show();
        }
        recAdapter.setSource("favorites");
    }

    @Override
    public void onStart() {
        super.onStart();
        recAdapter.setSource("favorites");
    }




}
