package si.uni_lj.fri.pbd.miniapp3.ui.search;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.Toast;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import si.uni_lj.fri.pbd.miniapp3.R;
import si.uni_lj.fri.pbd.miniapp3.adapter.RecyclerViewAdapter;
import si.uni_lj.fri.pbd.miniapp3.adapter.SpinnerAdapter;
import si.uni_lj.fri.pbd.miniapp3.models.Mapper;
import si.uni_lj.fri.pbd.miniapp3.models.RecipeSummaryIM;
import si.uni_lj.fri.pbd.miniapp3.models.dto.IngredientDTO;
import si.uni_lj.fri.pbd.miniapp3.models.dto.IngredientsDTO;
import si.uni_lj.fri.pbd.miniapp3.models.dto.RecipeSummaryDTO;
import si.uni_lj.fri.pbd.miniapp3.models.dto.RecipesByIngredientDTO;
import si.uni_lj.fri.pbd.miniapp3.rest.RestAPI;
import si.uni_lj.fri.pbd.miniapp3.rest.ServiceGenerator;

import static androidx.constraintlayout.widget.Constraints.TAG;

public class SearchFragment extends Fragment {

    private static final String TAG = "Search Fragment";

    //Initialize varaibles
    private RecyclerView recyclerView;
    private RecyclerView.LayoutManager layoutManager;
    private SpinnerAdapter adapter;
    private Spinner spinner;
    private RecyclerViewAdapter recAdapter;
    private String ingredient;
    View view;
    RestAPI service;
    ProgressBar bar;
    List<RecipeSummaryIM> recipeImList;
    SwipeRefreshLayout swipeRefreshLayout;
    private static boolean connected;
    private static boolean statusBefore;
    private static boolean statusAfter;

    //empty constructor
    public SearchFragment(){

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_search, container, false);
        //get the connection status
         connected=isConnectionAvailable(getContext());
         statusBefore=false;



        //Define variables
        recyclerView = view.findViewById(R.id.recycler_view);
        layoutManager = new LinearLayoutManager(view.getContext());
        recyclerView.setLayoutManager(layoutManager);

        //List of recipes in REcipeSummaryIM structure
        recipeImList=new ArrayList<RecipeSummaryIM>();

        //Setting the source for the adapter
        recAdapter.setSource("search");
        recAdapter=new RecyclerViewAdapter(getContext(),recipeImList,bar);
        recyclerView.setAdapter(recAdapter);

        spinner = (Spinner) view.findViewById(R.id.spinner);

        //Default ingridient
        ingredient="chicken";
        //Progress bar while loading images
        bar=view.findViewById(R.id.my_progressBar);
        bar.setVisibility(View.INVISIBLE);
        //Starting the service
        service = ServiceGenerator.createService(RestAPI.class);


           //Spinenr listener
           spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
               @Override
               public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                    if(isConnectionAvailable(getContext())) {
                        if (spinner.getItemAtPosition(position) != null) {
                            IngredientDTO temp = (IngredientDTO) spinner.getItemAtPosition(position);
                            ingredient = temp.getStrIngredient();
                            Log.i(TAG, ingredient);
                            if (bar != null) {
                                Log.i(TAG, "Bar visibility");
                                bar.setVisibility(View.VISIBLE);

                            }
                            //Show recipes after selecting an ingredient
                            Call<RecipesByIngredientDTO> recipe = service.getRecipes(ingredient);
                            recipe.enqueue(new Callback<RecipesByIngredientDTO>() {
                                @Override
                                public void onResponse(Call<RecipesByIngredientDTO> recipes, Response<RecipesByIngredientDTO> response) {
                                    generateRecipeList(response.body());

                                }

                                @Override
                                public void onFailure(Call<RecipesByIngredientDTO> recipes, Throwable t) {
                                    Toast.makeText(getContext(), "Something went wrong...Please try later!", Toast.LENGTH_SHORT).show();
                                }
                            });
                        } else {
                            Log.i(TAG, "null Object Meal");
                        }
                    }
                    else{
                        Toast.makeText(getContext(), "The device is not connected to the internet", Toast.LENGTH_LONG).show();
                    }

               }

               @Override
               public void onNothingSelected(AdapterView<?> parentView) {
                   // your code here
               }

           });


        return view;
    }

    public void whenConnected(){
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                //  Auto-generated method stub
                swipeRefreshLayout.setRefreshing(false);
            }
        }, 5000);

        //API call for all ingredients - for generating the spinner element
        Call<IngredientsDTO> call = service.getAllIngredients();
        call.enqueue(new Callback<IngredientsDTO>() {
            @Override
            public void onResponse(Call<IngredientsDTO> call, Response<IngredientsDTO> response) {
                swipeRefreshLayout = (SwipeRefreshLayout) view.findViewById(R.id.SwipeRefreshLayout);
                swipeRefreshLayout.setColorScheme(android.R.color.holo_blue_light);
                swipeRefreshLayout.setOnRefreshListener(refreshListener);
                generateDataList(response.body());

            }

            @Override
            public void onFailure(Call<IngredientsDTO> call, Throwable t) {
                Toast.makeText(getContext(), "Something went wrong...Please try later!", Toast.LENGTH_SHORT).show();
            }
        });



    }
    //Method for API calls for all ingrediets
    public void callRestAPI(RestAPI service){
        Call<IngredientsDTO> call= service.getAllIngredients();
        call.enqueue(new Callback<IngredientsDTO>() {
            @Override
            public void onResponse(Call<IngredientsDTO> call, Response<IngredientsDTO> response) {
                generateDataList(response.body());

            }

            @Override
            public void onFailure(Call<IngredientsDTO> call, Throwable t) {
                Toast.makeText(getContext(), "Something went wrong...Please try later!", Toast.LENGTH_SHORT).show();
            }
        });
    }
//Setting the resource for the adapter
    @Override
    public void onResume() {
        super.onResume();
        recAdapter.setSource("search");
        connected=isConnectionAvailable(getContext());

        if(connected ){
            if(statusBefore==false) {
                statusBefore = connected;
                whenConnected();
            }
        }
        else{
            Toast.makeText(getContext(), "No Conenction", Toast.LENGTH_LONG).show();
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        recAdapter.setSource("search");
        connected=isConnectionAvailable(getContext());

    }
  //generating the list for spinner
    private void generateDataList(IngredientsDTO ingredients) {

        adapter=new SpinnerAdapter(getContext(),ingredients);
        spinner.setAdapter(adapter);

    }
    //Generating all recipes

    private void generateRecipeList(RecipesByIngredientDTO recipes){

        List<RecipeSummaryDTO> recipeList=recipes.getRecipes();
        recipeImList=new ArrayList<RecipeSummaryIM>();
        for(int i=0; i<recipeList.size();i++){
            recipeImList.add(Mapper.mapRecipeSummaryDTOToRecipeSummaryIm(recipeList.get(i)));
        }
        recAdapter.setSource("search");
        recAdapter=new RecyclerViewAdapter(getContext(),recipeImList,bar);
        recyclerView.setAdapter(recAdapter);
    }
      //Swipe refresh Listenr
    private SwipeRefreshLayout.OnRefreshListener refreshListener = new SwipeRefreshLayout.OnRefreshListener() {

        @Override
        public void onRefresh() {
            swipeRefreshLayout.setRefreshing(true);
            callRestAPI(service);
            swipeRefreshLayout.setRefreshing(false);
        }
    };
  //check if the internet connection is available
    public  boolean isConnectionAvailable(Context context) {
        ConnectivityManager connectivity = (ConnectivityManager)context
                .getSystemService(Context.CONNECTIVITY_SERVICE);

        Log.i(TAG, "in connectionavailable");
        if(connectivity.getNetworkInfo(ConnectivityManager.TYPE_MOBILE).getState() == NetworkInfo.State.CONNECTED ||
                connectivity.getNetworkInfo(ConnectivityManager.TYPE_WIFI).getState() == NetworkInfo.State.CONNECTED) {
            Log.i(TAG, "in connectionavailableTrue");
             return true;
        }
        return false;

        }


}
