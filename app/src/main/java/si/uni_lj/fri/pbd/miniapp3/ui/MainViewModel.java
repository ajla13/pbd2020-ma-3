package si.uni_lj.fri.pbd.miniapp3.ui;

import android.app.Application;

import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import java.util.List;

import si.uni_lj.fri.pbd.miniapp3.database.entity.RecipeDetails;
import si.uni_lj.fri.pbd.miniapp3.models.RecipeSummaryIM;

public class MainViewModel extends AndroidViewModel {

    //Live data fields
    private LiveData<List<RecipeDetails>> allRecipes;
    private MutableLiveData<RecipeDetails>  searchResults;
    private  List<RecipeDetails> favorites;


   //Recipe repositorxy field
    private RecipeRepository repository;

    public MainViewModel (Application application) {
        super(application);


        repository=new RecipeRepository(application);
        //getting all all the recepies that will be shown in favorites fragment when updated
        allRecipes=repository.getFavorites();
        //getting all all the recepies that will be shown in favorites fragment when frist created
        favorites=repository.getFaveFragment();
        //getting the search results from findRecipe
        searchResults=repository.getSearchResults();

    }

  //getter functions
    MutableLiveData<RecipeDetails> getSearchResults() {
        return searchResults;
    }
    public LiveData<List<RecipeDetails>> getFavorites() {
        return allRecipes;
    }

   //query functions

    public void insertRecipe(RecipeDetails recipe) {
        repository.insertRecipe(recipe);
    }

    public void findRecipe(String idMeal) {
        repository.findRecipe(idMeal);

    }
    public void deleteRecipe(String idMeal) {
        repository.deleteRecipe(idMeal);
    }

    public List<RecipeDetails> getFaveFragment(){
        return favorites;
    }


}

