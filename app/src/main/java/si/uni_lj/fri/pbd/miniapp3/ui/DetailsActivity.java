package si.uni_lj.fri.pbd.miniapp3.ui;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;

import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import si.uni_lj.fri.pbd.miniapp3.R;
import si.uni_lj.fri.pbd.miniapp3.adapter.RecyclerViewAdapter;
import si.uni_lj.fri.pbd.miniapp3.database.entity.RecipeDetails;
import si.uni_lj.fri.pbd.miniapp3.models.Mapper;
import si.uni_lj.fri.pbd.miniapp3.models.RecipeDetailsIM;
import si.uni_lj.fri.pbd.miniapp3.models.RecipeSummaryIM;
import si.uni_lj.fri.pbd.miniapp3.models.dto.IngredientsDTO;
import si.uni_lj.fri.pbd.miniapp3.models.dto.RecipeDetailsDTO;
import si.uni_lj.fri.pbd.miniapp3.models.dto.RecipesByIdDTO;
import si.uni_lj.fri.pbd.miniapp3.rest.RestAPI;
import si.uni_lj.fri.pbd.miniapp3.rest.ServiceGenerator;
import si.uni_lj.fri.pbd.miniapp3.ui.search.SearchFragment;

public class DetailsActivity extends AppCompatActivity {

    private static final String TAG = "DetailsActivity";
    private String imageId;
    private String source;
    private MainViewModel mViewModel;
    private boolean fave;
    public List<RecipeSummaryIM> recipeSummaryList;
    public Bitmap bitmap;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_details);


        //get the source that activated the detials
        source=getIntent().getStringExtra("source");
        Log.i(TAG,source);
        mViewModel = new ViewModelProvider(this).get(MainViewModel.class);

        //Observer for notifying the favorites fragment when a change has been made and changing the list
        mViewModel.getFavorites().observe(this,
                new Observer<List<RecipeDetails>>() {
                    @Override
                    public void onChanged(@Nullable final List<RecipeDetails> recipes) {
                        recipeSummaryList=new ArrayList<RecipeSummaryIM>();
                        for(int i=0; i<recipes.size();i++){
                            recipeSummaryList.add(Mapper.mapRecipeDetailsToRecipeSummaryIm(recipes.get(i)));
                        }
                            new RecyclerViewAdapter().setRecipeList(recipeSummaryList);

                    }
                });
       //getting the id of the recipe that has been selected
        imageId = getIntent().getStringExtra("recipeId");
        mViewModel.findRecipe(imageId);
        //getting the recie that has been selected
        mViewModel.getSearchResults().observe(this,recipeDetails -> {
            if(recipeDetails!=null){
                fave=recipeDetails.getFavorite();
            }
            else{
                fave=false;
            }
        });

        //if the source is tha search fragment start an API call
        if(source.equals("Search")) {
            if (new SearchFragment().isConnectionAvailable(getApplicationContext())) {
                RestAPI service = ServiceGenerator.createService(RestAPI.class);
                Call<RecipesByIdDTO> call = service.getRecipe(imageId);
                call.enqueue(new Callback<RecipesByIdDTO>() {
                    @Override
                    public void onResponse(Call<RecipesByIdDTO> call, Response<RecipesByIdDTO> response) {
                        List<RecipeDetailsDTO> recipes = response.body().getRecipe();
                        RecipeDetailsDTO recipeDTO = recipes.get(0);
                        //map into RecipeDetailsIM model
                        RecipeDetailsIM recipe = Mapper.mapRecipeDetailsDtoToRecipeDetailsIm(
                                fave, recipeDTO);
                        generateDetails(recipe);
                    }

                    @Override
                    public void onFailure(Call<RecipesByIdDTO> call, Throwable t) {
                        Toast.makeText(getApplicationContext(), "Something went wrong...Please try later!", Toast.LENGTH_SHORT).show();
                    }
                });
            }
            else {
                Toast.makeText(getApplicationContext(), "The device is not connected to the Internet", Toast.LENGTH_SHORT).show();
            }
        }

        //if the source is from the favorites fetch data from database
        else if(source.equals("Favorites")){
            mViewModel.findRecipe(imageId);
            mViewModel.getSearchResults().observe(this,recipeDetails -> {
                if(recipeDetails!=null) {
                    RecipeDetailsIM recipe = Mapper.mapRecipeDetailsToRecipeDetailsIm(true, recipeDetails);
                    generateDetails(recipe);
                }
            });

        }

    }

    //show details of selcted recipe
    public void generateDetails(RecipeDetailsIM recipe){


        ImageView itemImage=(ImageView)findViewById(R.id.image_details);
        if(source.equals("Search")) {
            new DownloadImageTask(itemImage).execute(recipe.getStrMealThumb());
        }
        else{
            Bitmap bitImage=StringToBitMap(recipe.getImage());
            itemImage.setImageBitmap(bitImage);
        }

        TextView title=(TextView)findViewById(R.id.text_view_title);
        title.setText(recipe.getStrMeal());

        TextView ingredients=(TextView)findViewById(R.id.text_view_ingredients);
        String ing="";
        if(recipe.getStrIngredient1()!=null && !recipe.getStrIngredient1().equals("")){
            ing=recipe.getStrIngredient1();
        }
        if(recipe.getStrIngredient2()!=null && !recipe.getStrIngredient2().equals("")){
            ing=ing + " " + recipe.getStrIngredient2();
        }
        if(recipe.getStrIngredient3()!=null && !recipe.getStrIngredient3().equals("")){
            ing=ing+", "+recipe.getStrIngredient3();
        }
        if(recipe.getStrIngredient4()!=null && !recipe.getStrIngredient4().equals("")){
            ing=ing+", "+recipe.getStrIngredient4();
        }
        if(recipe.getStrIngredient5()!=null && !recipe.getStrIngredient5().equals("")){
            ing=ing+", "+recipe.getStrIngredient5();
        }
        if(recipe.getStrIngredient6()!=null && !recipe.getStrIngredient6().equals("")){
            ing=ing+", "+recipe.getStrIngredient6();
        }
        if(recipe.getStrIngredient7()!=null && !recipe.getStrIngredient7().equals("")){
            ing=ing+", "+recipe.getStrIngredient7();
        }
        if(recipe.getStrIngredient8()!=null && !recipe.getStrIngredient8().equals("")){
            ing=ing+", "+recipe.getStrIngredient8();
        }
        if(recipe.getStrIngredient9()!=null && !recipe.getStrIngredient9().equals("")){
            ing=ing+", "+recipe.getStrIngredient9();
        }
        if(recipe.getStrIngredient10()!=null && !recipe.getStrIngredient10().equals("")){
            ing=ing+", "+recipe.getStrIngredient10();
        }
        if(recipe.getStrIngredient11()!=null && !recipe.getStrIngredient11().equals("")){
            ing=ing+", "+recipe.getStrIngredient11();
        }
        if(recipe.getStrIngredient12()!=null && !recipe.getStrIngredient12().equals("")){
            ing=ing+", "+recipe.getStrIngredient12();
        }
        if(recipe.getStrIngredient13()!=null && !recipe.getStrIngredient13().equals("")){
            ing=ing+", "+recipe.getStrIngredient13();
        }
        if(recipe.getStrIngredient14()!=null && !recipe.getStrIngredient14().equals("")){
            ing=ing+", "+recipe.getStrIngredient14();
        }
        if(recipe.getStrIngredient15()!=null && !recipe.getStrIngredient15().equals("")){
            ing=ing+", "+recipe.getStrIngredient15();
        }
        if(recipe.getStrIngredient16()!=null && !recipe.getStrIngredient16().equals("")){
            ing=ing+", "+recipe.getStrIngredient16();
        }
        if(recipe.getStrIngredient17()!=null && !recipe.getStrIngredient17().equals("")){
            ing=ing+", "+recipe.getStrIngredient17();
        }
        if(recipe.getStrIngredient18()!=null && !recipe.getStrIngredient18().equals("")){
            ing=ing+", "+recipe.getStrIngredient18();
        }
        if(recipe.getStrIngredient19()!=null && !recipe.getStrIngredient19().equals("")){
            ing=ing+", "+recipe.getStrIngredient19();
        }

        if(recipe.getStrIngredient20()!=null && !recipe.getStrIngredient20().equals("")){
            ing=ing+", "+recipe.getStrIngredient20();
        }
        ingredients.setText(ing);


        TextView measure=(TextView)findViewById(R.id.text_view_measerments);
        String m="";
        if(recipe.getStrMeasure1()!=null && !recipe.getStrMeasure1().equals("")){
            m=recipe.getStrMeasure1();
        }
        if(recipe.getStrMeasure2()!=null && !recipe.getStrMeasure2().equals("")){
            m=m + ", " + recipe.getStrMeasure2();
        }
        if(recipe.getStrMeasure3()!=null && !recipe.getStrMeasure3().equals("")){
            m=m + ", " + recipe.getStrMeasure3();
        }
        if(recipe.getStrMeasure4()!=null && !recipe.getStrMeasure4().equals("")){
            m=m + ", " + recipe.getStrMeasure4();
        }
        if(recipe.getStrMeasure5()!=null && !recipe.getStrMeasure5().equals("")){
            m=m + ", " + recipe.getStrMeasure5();
        }
        if(recipe.getStrMeasure6()!=null && !recipe.getStrMeasure6().equals("")){
            m=m + ", " + recipe.getStrMeasure6();
        }

        if(recipe.getStrMeasure7()!=null && !recipe.getStrMeasure7().equals("")){
            m=m + ", " + recipe.getStrMeasure7();
        }
        if(recipe.getStrMeasure8()!=null && !recipe.getStrMeasure8().equals("")){
            m=m + ", " + recipe.getStrMeasure8();
        }
        if(recipe.getStrMeasure9()!=null && !recipe.getStrMeasure9().equals("")){
            m=m + ", " + recipe.getStrMeasure9();
        }
        if(recipe.getStrMeasure10()!=null && !recipe.getStrMeasure10().equals("")){
            m=m + ", " + recipe.getStrMeasure10();
        }
        if(recipe.getStrMeasure11()!=null && !recipe.getStrMeasure11().equals("")){
            m=m + ", " + recipe.getStrMeasure11();
        }
        if(recipe.getStrMeasure12()!=null && !recipe.getStrMeasure12().equals("")){
            m=m + ", " + recipe.getStrMeasure12();
        }
        if(recipe.getStrMeasure13()!=null && !recipe.getStrMeasure13().equals("")){
            m=m + ", " + recipe.getStrMeasure13();
        }
        if(recipe.getStrMeasure14()!=null && !recipe.getStrMeasure14().equals("")){
            m=m + ", " + recipe.getStrMeasure14();
        }
        if(recipe.getStrMeasure15()!=null && !recipe.getStrMeasure15().equals("")){
            m=m + ", " + recipe.getStrMeasure15();
        }
        if(recipe.getStrMeasure16()!=null && !recipe.getStrMeasure16().equals("")){
            m=m + ", " + recipe.getStrMeasure16();
        }
        if(recipe.getStrMeasure17()!=null && !recipe.getStrMeasure17().equals("")){
            m=m + ", " + recipe.getStrMeasure17();
        }
        if(recipe.getStrMeasure18()!=null && !recipe.getStrMeasure18().equals("")){
            m=m + ", " + recipe.getStrMeasure18();
        }
        if(recipe.getStrMeasure19()!=null && !recipe.getStrMeasure19().equals("")){
            m=m + ", " + recipe.getStrMeasure19();
        }

        if(recipe.getStrMeasure20()!=null && !recipe.getStrMeasure20().equals("")){
            m=m + ", " + recipe.getStrMeasure20();
        }
        measure.setText(m);

        TextView instruction=(TextView)findViewById(R.id.text_view_instructions);
        instruction.setText(recipe.getStrInstructions());

        Button favorite=(Button)findViewById(R.id.radioButton);
        if(recipe.getFavorite()){
            favorite.setText("Delete from favotites");
        }
        favorite.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if(recipe.getFavorite()){
                    recipe.setFavorite(false);
                    favorite.setText("Add to Favorites");
                    mViewModel.deleteRecipe(recipe.getIdMeal());
                }
                else{
                    String image=BitMapToString(bitmap);
                    recipe.setImage(image);
                    recipe.setFavorite(true);
                    Log.i(TAG,recipe.toString());
                    favorite.setText("Delete from Favorites");
                    RecipeDetails newRecipe=Mapper.mapRecipeDetailsImToRecipeDetails(true,recipe);
                    Log.i(TAG,newRecipe.toString());
                    mViewModel.insertRecipe(newRecipe);
                }
            }
        });

    }
//show image function
    private class DownloadImageTask extends AsyncTask<String, Void, Bitmap> {

        ImageView bmImage;
        public DownloadImageTask(ImageView bmImage) {
            this.bmImage = bmImage;
        }

        protected Bitmap doInBackground(String... urls) {
            String urldisplay = urls[0];
            Bitmap bmp = null;
            try {
                InputStream in = new java.net.URL(urldisplay).openStream();
                bmp = BitmapFactory.decodeStream(in);
            } catch (Exception e) {
                Log.e("Error", e.getMessage());
                e.printStackTrace();
            }
            bitmap=bmp;
            return bmp;
        }
        protected void onPostExecute(Bitmap result) {

            bmImage.setImageBitmap(result);
        }
    }

    public String BitMapToString(Bitmap bitmap){
        ByteArrayOutputStream baos=new  ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG,100, baos);
        byte [] arr=baos.toByteArray();
        String result= Base64.encodeToString(arr, Base64.DEFAULT);
        return result;
    }

    public Bitmap StringToBitMap(String image){
        try{
            byte [] encodeByte= Base64.decode(image,Base64.DEFAULT);
            Bitmap bitmap=BitmapFactory.decodeByteArray(encodeByte, 0, encodeByte.length);
            return bitmap;
        }catch(Exception e){
            e.getMessage();
            return null;
        }
    }
}
