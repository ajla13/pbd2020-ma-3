package si.uni_lj.fri.pbd.miniapp3.ui;

import android.os.Bundle;
import androidx.appcompat.widget.Toolbar;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.viewpager2.widget.ViewPager2;

import com.google.android.material.tabs.TabLayout;
import com.google.android.material.tabs.TabLayoutMediator;

import si.uni_lj.fri.pbd.miniapp3.R;


public class MainActivity extends AppCompatActivity {

    private static final String TAG = "MainActivity";

    private static final int NUM_OF_TABS = 2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        configureTabLayout();

    }

    //Tab - ViewPager Connection
    private void configureTabLayout(){

        TabLayout tabLayout= findViewById(R.id.tab_layout);
        ViewPager2 viewPager = findViewById(R.id.pager);
        SectionsPagerAdapter adapter= new SectionsPagerAdapter(this,NUM_OF_TABS);
        viewPager.setAdapter(adapter);

        new TabLayoutMediator(tabLayout, viewPager,
                new TabLayoutMediator.TabConfigurationStrategy() {
                    @Override public void onConfigureTab(@NonNull TabLayout.Tab
                                                                 tab, int position) {

                        //tab titles
                        switch(position){
                            case 0:
                                tab.setText("SEARCH RECIPE BY INGREDIENT");
                                break;
                            case 1:
                                tab.setText("FAVORITE RECIPES");
                                break;
                        }
                    }
                }).attach();


    }

}

