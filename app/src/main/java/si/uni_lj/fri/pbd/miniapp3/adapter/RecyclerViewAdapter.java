package si.uni_lj.fri.pbd.miniapp3.adapter;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.io.InputStream;
import java.util.List;

import si.uni_lj.fri.pbd.miniapp3.R;
import si.uni_lj.fri.pbd.miniapp3.models.RecipeDetailsIM;
import si.uni_lj.fri.pbd.miniapp3.models.RecipeSummaryIM;
import si.uni_lj.fri.pbd.miniapp3.models.dto.RecipeSummaryDTO;
import si.uni_lj.fri.pbd.miniapp3.models.dto.RecipesByIngredientDTO;
import si.uni_lj.fri.pbd.miniapp3.ui.DetailsActivity;
import si.uni_lj.fri.pbd.miniapp3.ui.search.SearchFragment;

public class RecyclerViewAdapter extends RecyclerView.Adapter<RecyclerViewAdapter.CardViewHolder> {

    Context context;
    LayoutInflater inflater;
    List<RecipeSummaryIM> rec;
    ProgressBar bar;
    static String imageId;
    private static final String TAG = " Recycle Adapter";
    private static String source;

    //define constructor

    public RecyclerViewAdapter(Context applicationContext, List<RecipeSummaryIM> recipes, ProgressBar bar) {
        this.context = applicationContext;
        inflater = (LayoutInflater.from(applicationContext));
        this.rec=recipes;
        this.bar=bar;


    }
    public RecyclerViewAdapter(){

    }
//get and set the source
    public static String getSource() {
        return source;
    }

    public static void setSource(String source) {
        RecyclerViewAdapter.source = source;
    }

    class CardViewHolder extends RecyclerView.ViewHolder {

        public ImageView itemImage;
        public TextView itemTitle;


        public CardViewHolder(View itemView) {
                super(itemView);
                itemImage = itemView.findViewById(R.id.image_view);
                itemTitle = itemView.findViewById(R.id.text_view_content);


        }
    }


    @NonNull
    @Override
    public CardViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.layout_grid_item, viewGroup, false);
        CardViewHolder vh = new CardViewHolder(v);
        return vh;

    }


    @Override
    public void onBindViewHolder(@NonNull CardViewHolder viewHolder, int i) {
        //convert the image and set the title
        SearchFragment searchFragment=new SearchFragment();

        if( getSource().equals("search")) {

            new DownloadImageTask(viewHolder.itemImage).execute(rec.get(i).getStrMealThumb());
            viewHolder.itemTitle.setText(rec.get(i).getStrMeal());

            if (viewHolder.itemImage != null) {
                viewHolder.itemImage.setOnClickListener(new View.OnClickListener() {

                    public void onClick(View v) {
                        imageId = rec.get(i).getIdMeal();
                        if (searchFragment.isConnectionAvailable(context)) {
                            //define the source for the details activity
                            Intent intent = new Intent(context, DetailsActivity.class);
                            intent.putExtra("recipeId", imageId);
                            intent.putExtra("source", "Search");

                            context.startActivity(intent);
                        }
                        else{
                            Toast.makeText(context, "The device is not connected to the internet,details not available", Toast.LENGTH_LONG).show();
                        }
                    }
                });

        }
            else{
                Toast.makeText(context, "The device is not connected to the internet!", Toast.LENGTH_LONG).show();
            }
        }
        else if(getSource().equals("favorites")){


            Bitmap image=StringToBitMap(rec.get(i).getImage());

            viewHolder.itemImage.setImageBitmap(image);
            viewHolder.itemTitle.setText(rec.get(i).getStrMeal());

            if (viewHolder.itemImage != null) {
                viewHolder.itemImage.setOnClickListener(new View.OnClickListener() {

                    public void onClick(View v) {
                        imageId = rec.get(i).getIdMeal();

                        //define the source for the details activity
                        Intent intent = new Intent(context, DetailsActivity.class);
                        intent.putExtra("recipeId", imageId);

                            intent.putExtra("source", "Favorites");

                        context.startActivity(intent);
                    }
                });
            }
        }

    }
  //notify the view if the list changes
    public void setRecipeList(List<RecipeSummaryIM> recipeList) {
        rec = recipeList;
        notifyDataSetChanged();
    }

    private class DownloadImageTask extends AsyncTask<String, Void, Bitmap> {

        ImageView bmImage;
        public DownloadImageTask(ImageView bmImage) {
            this.bmImage = bmImage;
        }

        protected Bitmap doInBackground(String... urls) {
            String urldisplay = urls[0];
            Bitmap bmp = null;
            try {
                InputStream in = new java.net.URL(urldisplay).openStream();
                bmp = BitmapFactory.decodeStream(in);
            } catch (Exception e) {
                Log.e("Error", e.getMessage());
                e.printStackTrace();
            }
            return bmp;
        }
        protected void onPostExecute(Bitmap result) {
            if(getSource().equals("search")) {
                //set a progress bar in search fragment
                if (bar != null) {
                    Log.i(TAG, "Bar invisibility");
                    bar.setVisibility(View.INVISIBLE);

                }
            }
            bmImage.setImageBitmap(result);
        }
    }

    @Override
    public int getItemCount() {
        return rec.size();

    }
    public Bitmap StringToBitMap(String image){
        try{
            byte [] encodeByte= Base64.decode(image,Base64.DEFAULT);
            Bitmap bitmap=BitmapFactory.decodeByteArray(encodeByte, 0, encodeByte.length);
            return bitmap;
        }catch(Exception e){
            e.getMessage();
            return null;
        }
    }
}
