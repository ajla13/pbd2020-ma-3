package si.uni_lj.fri.pbd.miniapp3.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Spinner;
import android.widget.TextView;

import java.util.List;

import si.uni_lj.fri.pbd.miniapp3.R;
import si.uni_lj.fri.pbd.miniapp3.models.dto.IngredientDTO;
import si.uni_lj.fri.pbd.miniapp3.models.dto.IngredientsDTO;

public class SpinnerAdapter extends BaseAdapter {
    Context context;
    LayoutInflater inflater;
    IngredientsDTO ingredients;
    List<IngredientDTO> allIng;

    //constructor

    public SpinnerAdapter(Context applicationContext,IngredientsDTO ing) {
        this.context = applicationContext;
        inflater = (LayoutInflater.from(applicationContext));
        this.ingredients=ing;
        this.allIng=this.ingredients.getIngredients();
    }

    @Override
    public int getCount() {
        return allIng.size();
    }

    @Override
    public Object getItem(int position) {
        return allIng.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    //set theingridient names in the spinener List
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        convertView = inflater.inflate(R.layout.spinner_item, null);
        TextView ingridient = (TextView) convertView.findViewById(R.id.text_view_spinner_item);
        ingridient.setText(allIng.get(position).getStrIngredient());
        return convertView;

    }


        }
