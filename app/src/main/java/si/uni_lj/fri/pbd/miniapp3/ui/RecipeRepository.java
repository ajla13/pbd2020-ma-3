package si.uni_lj.fri.pbd.miniapp3.ui;

import android.app.Application;
import android.provider.ContactsContract;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import java.util.List;

import si.uni_lj.fri.pbd.miniapp3.database.Database;
import si.uni_lj.fri.pbd.miniapp3.database.dao.RecipeDao;
import si.uni_lj.fri.pbd.miniapp3.database.entity.RecipeDetails;

public class RecipeRepository {

     //Initialize variables

    private MutableLiveData<RecipeDetails> searchResults =
            new MutableLiveData<>();
    private LiveData<List<RecipeDetails>> allRecipes;
    private  List<RecipeDetails> favorites;




    //DAO reference
    private RecipeDao recipeDao;

    //Repository  constructor
    public RecipeRepository(Application application) {
        //getting the database
        Database db;
        db = Database.getDatabase(application);
        recipeDao=db.recipeDao();
        //getting all all the recepies that will be shown in favorites fragment when updated
        allRecipes=recipeDao.getFavorites();
        //getting all all the recepies that will be shown in favorites fragment at the creation of the fragment
        favorites=recipeDao.getFaveFragment();
    }

    //querry for inserting a new recipe in favorites
    public void insertRecipe(final RecipeDetails newRecipe) {

        Database.databaseWriteExecutor.execute(new Runnable() {
            @Override
            public void run() {
                recipeDao.insertRecipe(newRecipe);
            }
        });

    }

    //querry for deleting a recipe from favorites
    public void deleteRecipe(final String idMeal) {
        Database.databaseWriteExecutor.execute(new Runnable() {
            @Override
            public void run() {
                recipeDao.deleteRecipe(idMeal);
            }
        });
    }

    //returning a selected recipe from favorites
    public void findRecipe(final String idMeal) {

        Database.databaseWriteExecutor.execute(new Runnable() {
            @Override
            public void run() {
                searchResults.postValue(recipeDao.getRecepiById(idMeal));
            }
        });


    }


 //getter methods

    public List<RecipeDetails> getFaveFragment(){
        return favorites;
    }
    public LiveData<List<RecipeDetails>> getFavorites() {
        return allRecipes;
    }

    public MutableLiveData<RecipeDetails> getSearchResults() {
        return searchResults;
    }
}
