package si.uni_lj.fri.pbd.miniapp3.ui;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;
import androidx.viewpager2.adapter.FragmentStateAdapter;

import si.uni_lj.fri.pbd.miniapp3.adapter.RecyclerViewAdapter;
import si.uni_lj.fri.pbd.miniapp3.ui.favourites.FavoritesFragment;
import si.uni_lj.fri.pbd.miniapp3.ui.search.SearchFragment;

public class SectionsPagerAdapter extends FragmentStateAdapter {

    private int tabCount;
    @NonNull
    @Override

    //returning the appropritae fragments
    public Fragment createFragment(int position) {

        if(position==0){

           return new SearchFragment();
        }
        else {

            return new FavoritesFragment();
        }

    }

    @Override
    public int getItemCount() {
        return tabCount;
    }
    public SectionsPagerAdapter(FragmentActivity fa, int numOfTabs){
        super(fa);
        tabCount=numOfTabs;
    }
}
